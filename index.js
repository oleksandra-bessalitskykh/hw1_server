const express = require('express');
const morgan = require('morgan');
const fs = require('fs');
const app = express();

app.use(function (request, responce, next) {
    responce.header("Access-Control-Allow-Origin", null);
    responce.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(express.json());
app.use(morgan('tiny'));

app.post('/api/files', (request, responce) => {
    const filename = request.body.filename;
    const content = request.body.content;

    fs.writeFile('files/' + filename, content, 'utf-8', error => {
        if (error) {
            responce.status(500).json({"message": "Server error"});
        } else if (!(filename && content)) {
            responce.status(400).json({"message": "Please specify 'content' parameter"});
        } else {
            responce.json({"message": 'File created successfully'})
        }
    });
});

app.get('/api/files', (request, responce) => {

    fs.readdir(__dirname + '/files', (error, files) => {
        if (error) {
            responce.status(500).json({"message": "Server error"});
        } else {
            responce.json({"message": 'Success', files});
        }
    });
});

app.get('/api/files/:filename', (request, response) => {
    let uploadedDate;

    fs.stat('files/' + request.params.filename, (error, stat) => {

        if (error) {
            response.status(500).json({"message": "Server error"});
        } else {
            uploadedDate = stat.ctime;
        }

    });

    fs.readFile('files/' + request.params.filename, 'utf8', (error, data) => {

        if (error) {
            response.status(500).json({"message": "Server error"});
        } else {
            response.json({
                "message": "Success",
                "filename": request.params.filename,
                "content": data,
                "extension": request.params.filename.split('.')[1],
                uploadedDate
            });
        }

    });
})

app.listen(8080, () => {
    const dir = 'files';

    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }

    console.log('Success');
});
